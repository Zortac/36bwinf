#include <iostream>
#include <fstream>
#include "PoolEntrance.h"
#include "json.hpp"

using namespace std;

// Declare price calculation function

int main() {
    // Create ticket type struct
    const vector<PoolEntrance::TicketType> ticket_types = {
            // Regular ticket for anyone above 16
            PoolEntrance::TicketType{3.5, "Normales Ticket",
                    age_restricted: true, min_age: 17},

            // Ticket for children and adolescents between 5 and 16
            PoolEntrance::TicketType{2.5, "Jugendlicher",
                    age_restricted: true, min_age: 5, max_age: 16},

            // Ticket for anyone 4 and below, must be accompanied by someone over 16
            PoolEntrance::TicketType{0, "Kleinkind",
                    age_restricted: true, 0, max_age: 4,
                    companion_restricted: true, companion_min_age: 16},

            // Standard group ticket, 6 persons 5 and above, only valid on weekdays
            PoolEntrance::TicketType{11, "Gruppenticket", false, 0, INT32_MAX, false, 0,
                    day_restricted: true, valid_on: PoolEntrance::WEEKDAY | PoolEntrance::HOLIDAY_BITS_MASK,
                    group_ticket: true, age_groups: {PoolEntrance::AgeGroup{6, 5, INT32_MAX}}},

            // Family ticket, 1 adult and 3 children between 5 and 16 (younger children are free anyway)
            PoolEntrance::TicketType{8, "Familienticket, 1 Erwachsener", false, 0, INT32_MAX, false, 0,
                                     false, 0,
                    group_ticket: true, age_groups: {PoolEntrance::AgeGroup{1, 18, INT32_MAX},
                                                     PoolEntrance::AgeGroup{3, 5, 16}}},

            // Family ticket, 2 adults and 2 children
            PoolEntrance::TicketType{8, "Familienticket, 2 Erwachsene", false, 0, INT32_MAX, false, 0,
                                     false, 0,
                    group_ticket: true, age_groups: {PoolEntrance::AgeGroup{2, 18, INT32_MAX},
                                                     PoolEntrance::AgeGroup{2, 5, 16}}}
    };

    const PoolEntrance::Rebates rebates = {
            single_ticket_rebates: {
                    {PoolEntrance::WEEKDAY | PoolEntrance::HOLIDAY_BITS_MASK, 0.2}
            },
            coupons_valid_day: PoolEntrance::SCHOOLDAY,
            coupon_group_rebate: 0.1
    };

    PoolEntrance poolEntrance = PoolEntrance(&ticket_types, &rebates);

    cout << "36. BWInf Runde 1 Aufg. 2: Schwimmbad, Team: Don't Panic! ID: 00899" << endl
         << "Name der Eingabe-Datei (Dateiformat siehe Doku): ";
    string input_file;
    cin >> input_file;

    fstream ifs(input_file, fstream::in | fstream::out);
    if (ifs.is_open()) {
        nlohmann::json in_obj;
        ifs >> in_obj;

        // Determine day
        PoolEntrance::DayType day;
        if (in_obj.at("weekend")) {
            day = PoolEntrance::WEEKEND;
        } else {
            day = PoolEntrance::WEEKDAY;
        }

        if (in_obj.at("holiday")) {
            day = day | PoolEntrance::HOLIDAY;
        } else {
            day = day | PoolEntrance::SCHOOLDAY;
        }

        // Get number of coupons
        int num_coupons = in_obj.at("num_coupons");

        vector<PoolEntrance::Person> persons;
        nlohmann::json in_persons = in_obj.at("customers");
        for (auto in_person : in_persons) {
            string name = in_person.at("name");
            int age = in_person.at("age");
            persons.emplace_back(PoolEntrance::Person(name, age));
        }

        sort(persons.begin(), persons.end());
        string cmp_str;
        int num = 0;
        for (auto &person : persons) {
            if (person.first == cmp_str) {
                num++;
                person.first += to_string(num);
            } else {
                cmp_str = person.first;
                num = 0;
            }
        }

        // Check that there are persons, otherwise assume invalid input.
        if (!persons.empty()) {
            // Set up customers
            PoolEntrance::PoolCustomers customers = {
                    day,
                    num_coupons,
                    persons
            };

            // Set up return variables to allow for boolean function (success check)
            vector<string> tickets;
            float price = 0;

            // Calculate prices and output
            if (poolEntrance.calculatePrices(&customers, &tickets, &price)) {
                cout << "Der Gesamtpreis betraegt " << price << "." << endl
                     << "Die folgenden Tickets sind  zu kaufen:" << endl;

                // Iterate through tickets, const reference to avoid copying
                for (const auto &ticket : tickets) {
                    cout << ticket << endl;
                }

            } else {
                cout << "Die Gruppe kann das Schwimmbad nicht geschlossen betreten." << endl;
            }
        } else {
            cout << "Eingabefehler. Terminiere..." << endl;
        }
    } else {
        cout << "Eingabefehler. Terminiere..." << endl;
    }

    system("pause");

    return 0;
}
