//
// Created by Zortac on 18.11.2017.
//

// Include guards to prevent cyclic inclusion
#ifndef BWINF_RD1_AUFG2_POOLENTRANCE_H
#define BWINF_RD1_AUFG2_POOLENTRANCE_H

// Libraries (only C++14 standard libraries)
#include <map>
#include <tuple>
#include <vector>

using namespace std;

class PoolEntrance {
public:

    typedef enum {
        WEEKDAY = 0b00000001,
        WEEKEND = 0b00000010,
        HOLIDAY = 0b00010000,
        SCHOOLDAY = 0b00100000,
        HOLIDAY_BITS_MASK = 0b11110000,
        WEEKDAY_BITS_MASK = 0b00001111
    } DayTypeMask;

    typedef int DayType;

    struct AgeGroup {
        int num;
        int min_age;
        int max_age;
    };

    // Restriction struct for pool prices
    struct TicketType {
        float price = 0;

        string output_name = "";

        // Any age restrictions?
        bool age_restricted = false;
        int min_age = 0;
        int max_age = INT32_MAX;

        // Must there be a companion of a certain age to be granted entrance (e.g. for little children)?
        bool companion_restricted = false;
        int companion_min_age = 0;

        bool day_restricted = false;
        DayType valid_on = 0;

        // Is this a group ticket?
        bool group_ticket = false;
        // List of group member classifications
        vector<AgeGroup> age_groups;

    };

    struct Rebates {
        vector<pair<DayType, float>> single_ticket_rebates;

        DayType coupons_valid_day;

        float coupon_group_rebate;
    };

    // Constructor for the PoolEntrance class
    PoolEntrance(const vector<TicketType> *prices, const Rebates *rebates);


    // Destructor, deletes pointers to avoid memory leaks
    ~PoolEntrance();

    typedef pair<string, int> Person;

    // Data struct to hold information about guests and circumstances
    struct PoolCustomers {
        // Type of day
        DayType day_type;
        // Number of coupons
        int num_coupons;
        // Name and age
        vector<Person> persons;
    };

    bool calculatePrices(PoolCustomers *customers, vector<string> *ret_ticket_list, float *ret_final_price);

private:
    // Base price list (reference to avoid copying), contains prices w/o discounts, see typedef for details
    const vector<TicketType> *_tickets;
    vector<const TicketType *> _single_tickets;
    vector<const TicketType *> _group_tickets;

    const Rebates *_rebates;

    // Ticket restriction checkers
    bool _dayValid(bool day_restricted, DayType day, DayType valid_on);
    bool _ageValid(bool age_restricted, int age, int min, int max);

    bool _companionValid(const vector<Person> *group, int age);

    bool _findBasePrice(const Person *person, const PoolCustomers *customers,
                        pair<float, pair<Person, const TicketType *>> *ret_base_price);

    bool _findCombiningGroupTickets(vector<const TicketType *> *valid_gtickets,
                                    multimap<float, pair<Person, const TicketType *>> *persons,
                                    vector<pair<const TicketType *, vector<Person>>> *ret_combining_tickets,
                                    float *ret_price_reduction);
};


#endif //BWINF_RD1_AUFG2_POOLENTRANCE_H
