//
// Created by Moritz F. Kuntze (Zortac) for team "Don't Panic!" for BWInf17 on 18.11.2017.
//

#include <iostream>
#include "PoolEntrance.h"

using namespace std;

// PoolEntrance constructor
PoolEntrance::PoolEntrance(const vector<TicketType> *prices, const Rebates *rebates) : _tickets(prices),
                                                                                       _rebates(rebates) {
    // Get list of single and group tickets
    for (const auto &_bp : *_tickets) {
        if (_bp.group_ticket) {
            _group_tickets.push_back(&_bp);
        } else {
            _single_tickets.push_back(&_bp);
        }
    }
}

// Implementation of the PoolEntrance destructor
PoolEntrance::~PoolEntrance() {
    // Delete _tickets
    _tickets = nullptr;
    delete _tickets;
}

bool PoolEntrance::calculatePrices(PoolEntrance::PoolCustomers *customers,
                                   vector<string> *ret_ticket_list, float *ret_final_price) {

    // Calculate base prices for each person
    multimap<float, pair<Person, const TicketType *>> base_prices;

    for (const auto& person : customers->persons) {
        pair<float, pair<Person, const TicketType *>> base_price;
        if (_findBasePrice(&person, customers, &base_price)) {
            base_prices.insert(base_price);
        } else {
            return false;
        }
    }

    // Filter out invalid group tickets before applying group tickets
    vector<const TicketType *> valid_group_tickets;
    for (const auto &gt : _group_tickets) {
        if (_dayValid(gt->day_restricted, customers->day_type, gt->valid_on)) {
            valid_group_tickets.push_back(gt);
        }
    }

    vector<pair<const TicketType *, vector<Person>>> combining_tickets;
    float price_reduction;

    bool price_reduction_possible = _findCombiningGroupTickets(&valid_group_tickets, &base_prices,
                                                               &combining_tickets, &price_reduction);

    if (customers->day_type & _rebates->coupons_valid_day && customers->num_coupons) {
        // Coupons are valid and are available
        if (customers->num_coupons >= base_prices.size()) {
            // More coupons than customers, everyone gets in for free, return
            *ret_final_price = 0;
            return true;
        }

        // Apply coupons definitely available for use on persons
        for (; customers->num_coupons > 1; customers->num_coupons--) {
            // Find highest regular paying person in possible group tickets and the group ticket they are in
            pair<float, Person> highest_paying_person = make_pair(0, Person());
            pair<const TicketType *, vector<Person>> highest_group_ticket;
            for (const auto &combining_ticket : combining_tickets) {
                for (const auto &person : combining_ticket.second) {
                    pair<float, pair<Person, const TicketType *>> price;
                    _findBasePrice(&person, customers, &price);
                    if (price.first > highest_paying_person.first) {
                        highest_paying_person = make_pair(price.first, price.second.first);
                        highest_group_ticket = combining_ticket;
                    }
                }
            }

            // Find remaining persons outside of this group ticket
            auto single_prices = base_prices;
            for (auto it = single_prices.begin(); it != single_prices.end(); it++) {
                for (const auto &person : highest_group_ticket.second) {
                    if (it->second.first == person) {
                        it = single_prices.erase(it);
                        it--;
                        break;
                    }
                }
            }

            if (!single_prices.empty() && single_prices.rbegin()->first >= highest_paying_person.first) {

                // Find this highest paying person in the list of prices
                auto possible_persons_range = base_prices.equal_range(single_prices.rbegin()->first);
                for (auto it = possible_persons_range.first; it != possible_persons_range.second; it++) {
                    if (it->first == single_prices.rbegin()->first) {
                        // Found! Remove them from from the ticket list lists as they won't be relevant to any calculations in the future
                        base_prices.erase(it);
                        break;
                    }
                }
            } else {
                // Highest paying person is in group (also true, when there are no more persons outside a group left)
                if (highest_paying_person.first > price_reduction) {
                    // Removing the highest paying person from the group would yield a lower price than applying the group ticket
                    // Find that highest paying person
                    auto possible_persons_range = base_prices.equal_range(highest_paying_person.first);
                    for (auto it = possible_persons_range.first; it != possible_persons_range.second; it++) {
                        if (it->second.first == highest_paying_person.second) {
                            // Remove them from any lists
                            base_prices.erase(it);
                            break;
                        }
                    }
                } else {
                    // Applying the group ticket would yield the same or a better reduction than removing the highest paying person from the group => save the coupon
                    // This group ticket is final. Add it to the list of tickets to be bought and add its price to the final price
                    ret_ticket_list->push_back(highest_group_ticket.first->output_name);
                    *ret_final_price += highest_group_ticket.first->price;
                    // Delete the persons from this group ticket out of ticket lists, as they are irrelevant to further calculations
                    for (const auto &person : highest_group_ticket.second) {
                        for (auto it = base_prices.begin(); it != base_prices.end();) {
                            if (it->second.first == person) {
                                it = base_prices.erase(it);
                            } else {
                                it++;
                            }
                        }
                    }
                    // Increment number of coupons (will be decremented before next iteration) as no coupon has been used
                    customers->num_coupons++;
                }

                // Re-calculate possible groups
                price_reduction_possible = _findCombiningGroupTickets(&valid_group_tickets, &base_prices,
                                                                      &combining_tickets, &price_reduction);
            }
        }

        vector<tuple<const TicketType *, vector<Person>, float>> grouped_persons;
        while (price_reduction_possible) {
            // Find grouping with least people to leave more ungrouped people for next grouping iterations
            pair<const TicketType *, vector<Person>> best_grouping = combining_tickets[0];
            for (const auto &combining_ticket : combining_tickets) {
                if (combining_ticket.second.size() < best_grouping.second.size()) {
                    best_grouping = combining_ticket;
                }
            }

            // Delete persons in best grouping from base_prices vector, as they are irrelevant for further groupings but add them to grouped_persons vector for later reference
            vector<Person> person_vec;
            for (const auto &person : best_grouping.second) {
                for (auto it = base_prices.begin(); it != base_prices.end();) {
                    if (it->second.first == person) {
                        person_vec.push_back(it->second.first);
                        it = base_prices.erase(it);
                    } else {
                        it++;
                    }
                }
            }
            grouped_persons.push_back(make_tuple(best_grouping.first, person_vec, price_reduction));

            // Re-calculate possible groupings
            price_reduction_possible = _findCombiningGroupTickets(&valid_group_tickets, &base_prices,
                                                                  &combining_tickets, &price_reduction);
            ret_ticket_list->push_back(best_grouping.first->output_name);
            *ret_final_price += best_grouping.first->price;
        }

        // At this point only one coupon is left and all possible groupings have been applied
        // Add all remaining ungrouped persons to return data
        for (const auto &person : base_prices) {
            ret_ticket_list->push_back(person.second.second->output_name);
            *ret_final_price += person.first;
        }

        // Calculate the possible rebate if the coupon is applied to the whole group
        float possible_group_rebate = *ret_final_price * _rebates->coupon_group_rebate;

        // Find the highest paying person from the groupings resulting in a lower change than the whole group rebate would
        pair<float, Person> highest_paying_person = make_pair(0, Person());
        tuple<const TicketType *, vector<Person>, float> highest_group_ticket;
        for (const auto &ticket : grouped_persons) {
            if (get<2>(ticket) < possible_group_rebate) {
                for (const auto &person : get<1>(ticket)) {
                    pair<float, pair<Person, const TicketType *>> price;
                    _findBasePrice(&person, customers, &price);
                    if (price.first > highest_paying_person.first) {
                        highest_paying_person = make_pair(price.first, price.second.first);
                        highest_group_ticket = ticket;
                    }
                }
            }
        }

        // Check if applying the coupon to this highest paying person would yield in a greater price reduction than applying it to the whole group
        // Also check that  the highest paying ungrouped person is paying less than the highest paying grouped person
        if (highest_paying_person.first > possible_group_rebate &&
            base_prices.rbegin()->first < highest_paying_person.first) {
            for (const auto &person : get<1>(highest_group_ticket)) {
                if (person != highest_paying_person.second) {
                    pair<float, pair<Person, const TicketType *>> price;
                    _findBasePrice(&person, customers, &price);
                    ret_ticket_list->push_back(price.second.second->output_name);
                    *ret_final_price += price.first;
                }
            }
            for (auto it = ret_ticket_list->begin(); it != ret_ticket_list->end(); it++) {
                if (*it == get<0>(highest_group_ticket)->output_name) {
                    ret_ticket_list->erase(it);
                    *ret_final_price -= get<0>(highest_group_ticket)->price;
                    break;
                }
            }
        } else if (base_prices.rbegin()->first >= possible_group_rebate) {
            // Ungrouped person is paying more than the possible reduction to the whole group
            for (auto it = ret_ticket_list->begin(); it != ret_ticket_list->end(); it++) {
                if (*it == base_prices.rbegin()->second.second->output_name) {
                    ret_ticket_list->erase(it);
                    *ret_final_price -= base_prices.rbegin()->first;
                    break;
                }
            }
        } else {
            *ret_final_price -= possible_group_rebate;
            ret_ticket_list->push_back("Gutschein auf ganze Gruppe");
        }

        return true;
    } else {
        // Coupons aren't valid or there are none available
        while (price_reduction_possible) {
            // Find grouping with least people to leave more ungrouped people for next grouping iterations
            pair<const TicketType *, vector<Person>> best_grouping = combining_tickets[0];
            for (const auto &combining_ticket : combining_tickets) {
                if (combining_ticket.second.size() < best_grouping.second.size()) {
                    best_grouping = combining_ticket;
                }
            }

            // Delete persons in best grouping from base_prices vector, as they should not be included in further groupings
            for (const auto &person : best_grouping.second) {
                for (auto it = base_prices.begin(); it != base_prices.end();) {
                    if (it->second.first == person) {
                        it = base_prices.erase(it);
                    } else {
                        it++;
                    }
                }
            }

            // Add grouping to return data
            ret_ticket_list->push_back(best_grouping.first->output_name);
            *ret_final_price += best_grouping.first->price;

            // Re-calculate possible groupings
            price_reduction_possible = _findCombiningGroupTickets(&valid_group_tickets, &base_prices,
                                                                  &combining_tickets, &price_reduction);
        }

        // Add all remaining ungrouped persons to return data
        for (const auto &person : base_prices) {
            ret_ticket_list->push_back(person.second.second->output_name);
            *ret_final_price += person.first;
        }

        return true;
    }
}

bool PoolEntrance::_dayValid(bool day_restricted, PoolEntrance::DayType day, PoolEntrance::DayType valid_on) {
    if (!day_restricted) {
        return true;
    } else if ((valid_on & WEEKDAY_BITS_MASK & day) && (valid_on & HOLIDAY_BITS_MASK & day)) {
        return true;
    }
    return false;
}

bool PoolEntrance::_ageValid(bool age_restricted, int age, int min, int max) {
    if (!age_restricted) {
        return true;
    } else if (age >= min && age <= max) {
        return true;
    }
    return false;
}

bool PoolEntrance::_companionValid(const vector<Person> *group, int age) {
    for (const auto& person : *group) {
        if (person.second >= age) {
            return true;
        }
    }
    return false;
}

bool PoolEntrance::_findBasePrice(const PoolEntrance::Person *person, const PoolEntrance::PoolCustomers *customers,
                                  pair<float, pair<Person, const TicketType *>> *ret_base_price) {
    // Go over tickets to find matching ticket
    for (const auto &ticket_type : _single_tickets) {
        // Find the matching ticket to person
        // Check if day and age are valid
        if (_dayValid(ticket_type->day_restricted, customers->day_type, ticket_type->valid_on) &&
            _ageValid(ticket_type->age_restricted, person->second, ticket_type->min_age, ticket_type->max_age)) {
            // Check that ticket is either not restricted regarding companions or the companions are valid
            if (!ticket_type->companion_restricted ||
                _companionValid(&customers->persons, ticket_type->companion_min_age)) {
                // Correct ticket has been found
                // Apply daily rebate if applicable
                float price = ticket_type->price;
                for (const auto &rebate : _rebates->single_ticket_rebates) {
                    if (_dayValid(true, customers->day_type, rebate.first)) {
                        price -= price * rebate.second;
                    }
                }
                // Set return reference to pair of Person and price
                *ret_base_price = make_pair(price, make_pair(*person, ticket_type));
                // Return true, as ticket has been be found
                return true;
            } else {
                // Companion invalid, break and return as group cannot enter
                break;
            }
        }
    }
    // Return false, as no valid ticket could be found for this person
    return false;
}

bool PoolEntrance::_findCombiningGroupTickets(vector<const TicketType *> *valid_gtickets,
                                              multimap<float, pair<PoolEntrance::Person, const PoolEntrance::TicketType *>> *persons,
                                              vector<pair<const PoolEntrance::TicketType *, vector<PoolEntrance::Person>>> *ret_combining_tickets,
                                              float *ret_price_reduction) {
    // Zero out return variables
    *ret_combining_tickets = vector<pair<const PoolEntrance::TicketType *, vector<PoolEntrance::Person>>>();
    *ret_price_reduction = 0;

    // Map (ordered structure) of how each type of group ticket could be filled most efficiently
    // and which price reduction would result in using that ticket instead
    multimap<float, pair<const TicketType *, vector<Person>>> possible_combinations = multimap<float, pair<const TicketType *, vector<Person>>>();

    // Loop through valid group tickets to find optimal population of ticket for each
    for (const auto &gt : *valid_gtickets) {
        // List of age groups to be populated
        vector<pair<AgeGroup, vector<Person>>> age_groups = vector<pair<AgeGroup, vector<Person>>>();
        for (const auto &age_group : gt->age_groups) {
            age_groups.emplace_back(make_pair(age_group, vector<Person>()));
        }

        // Float to track price of combined single tickets to compare against group ticket price later
        float single_combined_price = 0;
        vector<Person> person_vec = vector<Person>();
        // Walk through each age group to populate if with persons
        for (auto &age_group : age_groups) {
            // Iterate through tickets backwards to get highest costs first
            for (auto r_it = persons->rbegin(); r_it != persons->rend(); r_it++) {
                // Get age of person
                int age = r_it->second.first.second;

                // If person fits in age group (by age and number), add to that group
                if (_ageValid(true, age, age_group.first.min_age, age_group.first.max_age) &&
                    age_group.second.size() < age_group.first.num) {
                    age_group.second.push_back(r_it->second.first);
                    person_vec.push_back(r_it->second.first);
                    single_combined_price += r_it->first;
                }
            }
        }

        // Insert this ticket to list of possible combinations
        pair<float, pair<const TicketType *, vector<Person>>> combination = make_pair(single_combined_price - gt->price,
                                                                                      make_pair(gt, person_vec));
        possible_combinations.insert(combination);
    }


    // Check if there are possible combinations
    if (!possible_combinations.empty()) {
        // Check that the highest possible price reduction is greater than 0
        if (possible_combinations.rbegin()->first > 0) {
            // Get range of all price reductions equal to the highest one
            auto highest_reductions = possible_combinations.equal_range(possible_combinations.rbegin()->first);

            // Write this range to the return vector pointer ret_combining_tickets
            for (auto it = highest_reductions.first; it != highest_reductions.second; it++) {
                ret_combining_tickets->emplace_back(it->second);
            }

            // Write price reduction achieved to return pointer
            *ret_price_reduction = possible_combinations.rbegin()->first;
            return true;
        }
    }
    return false;
}
