# Pool tickets

This was my submission for the first round of the 36th BWInf (Bundeswettbewerb Informatik, federal
computer science competition). It is a program to compute the minimal price for
pool entry of a group of people, based on some complex, but configurable, rules.

You can find the original task in German [here](https://bwinf.de/fileadmin/user_upload/BwInf/2017/36/1._Runde/Aufgaben/BWINF_36_Aufgaben.pdf) and the PDF documentation of the program (German, too) [here](https://zortac.net/content/static/bwinf/aufgabe2.pdf).

Example datasets can be found in the `data/` directory.
